package ru.mirea.security;

import org.apache.logging.log4j.*;
import java.math.BigInteger;

public class Eve {
    private static final Logger log = LogManager.getLogger(Eve.class);

    private static void doRSA() {
        log.info("Eve: Hello again! My name is Eve and i still transfer messages between Alice and Bob.");
        Alice alice = new Alice(64);
        Bob bob = new Bob(16);
        log.info("Eve: Sending key from Alice to Bob.");
        BigInteger[] key = alice.getPublicKey();
        log.info("Eve see value: [" + key[0] + ", " + key[1] + "].");
        log.info("Eve: Sending message from Bob to Alice.");
        BigInteger encryptedMessage = bob.encrypt(key);
        log.info("Eve see value: " + encryptedMessage);
        alice.decrypt(encryptedMessage);
    }

    public static void main(String[] args) {
        doRSA();
        System.out.println("Log has been saved to \"./RSA.log\" successfully.");
    }
}
