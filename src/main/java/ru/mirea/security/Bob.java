package ru.mirea.security;

import org.apache.logging.log4j.*;
import java.math.BigInteger;
import java.util.Random;

class Bob {
    private static final Logger log = LogManager.getLogger(Bob.class);

    private BigInteger message;

    public Bob(int bitLength) {
        Random random = new Random();

        this.message = new BigInteger(bitLength, random);
    }

    public BigInteger encrypt(BigInteger[] key) {
        log.info("Bob: Sending message \'"
                + this.message +
                "\' for Alice encrypted with public key.");

        BigInteger exponent = key[0];
        BigInteger modulus = key[1];

        return message.modPow(exponent, modulus);
    }
}
