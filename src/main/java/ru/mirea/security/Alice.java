package ru.mirea.security;

import org.apache.logging.log4j.*;
import java.math.BigInteger;
import java.util.Random;

class Alice {
    private static final Logger log = LogManager.getLogger(Alice.class);

    private int bitLength;

    private Random random = new Random();

    private BigInteger modulus;
    private BigInteger publicKey;
    private BigInteger privateKey;

    public Alice(int bitLength) {
        this.bitLength = bitLength;

        BigInteger p = BigInteger.probablePrime(bitLength/2, random).abs();
        BigInteger q = BigInteger.probablePrime(bitLength/2, random).abs();
        BigInteger eulerFunction = eulerFunction(p, q);

        this.modulus = p.multiply(q);
        this.privateKey = generatePrivateExponent(eulerFunction);
        this.publicKey = generatePublicExponent(eulerFunction, privateKey);
    }

    public BigInteger[] getPublicKey() {
        log.info("Alice: Sending public key for Bob.");

        return new BigInteger[] {publicKey, modulus};
    }

    public void decrypt(BigInteger message) {
        log.info("Alice: Decrypting received message.");
        log.info("I decrypt Bob's message: " + message.modPow(privateKey, modulus) + ".");
    }

    private BigInteger eulerFunction(BigInteger p, BigInteger q) {
        return p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
    }

    private BigInteger generatePublicExponent(BigInteger eulerFunction, BigInteger privateExponent) {
        log.info("Alice: Generating public key.");

        return privateExponent.modInverse(eulerFunction);
    }

    private BigInteger generatePrivateExponent(BigInteger primeNumber) {
        log.info("Alice: Generating private key.");

        BigInteger privateKey = new BigInteger(bitLength, random);

        while (!primeNumber.gcd(privateKey).equals(BigInteger.ONE))
            privateKey = new BigInteger(bitLength, random);

        return privateKey;
    }
}


